﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestToDoListWebApi.Models;
using TestToDoListWebApi.Repositories.Interfaces;

namespace TestToDoListWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskItemController : ControllerBase
    {
        private ITaskItemRepository _taskItemRepository;

        public TaskItemController(ITaskItemRepository taskItemRepository)
        {
            _taskItemRepository = taskItemRepository;
        }

        // GET: api/TaskItem
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskItem>>> GetTaskItems()
        {
            return await _taskItemRepository.GetAll().ToListAsync();
        }

        // GET: api/TaskItem/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskItem>> GetTaskItem(long id)
        {
            var taskItem = await _taskItemRepository.GetById(id);

            if (taskItem == null)
            {
                return NotFound();
            }

            return taskItem;
        }

        // GET: api/TaskItem/user/454355
        [HttpGet("user/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskItem>>> GetUserTaskItems(long userId)
        {
            return await _taskItemRepository.GetByCondition(taskItem => taskItem.UserId == userId).ToListAsync();
        }

        // POST: api/TaskItem
        [HttpPost]
        public async Task<ActionResult<TaskItem>> PostTaskItem(TaskItem taskItem)
        {
            try
            {
                _taskItemRepository.Create(taskItem);
                await _taskItemRepository.SaveAsync();
                return CreatedAtAction(nameof(GetTaskItem), new { id = taskItem.Id }, taskItem);
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }

        // PUT: api/TaskItem/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTaskItem(long id, TaskItem taskItem)
        {
            if (id != taskItem.Id)
            {
                return BadRequest();
            }

            _taskItemRepository.Update(taskItem);
            await _taskItemRepository.SaveAsync();

            return NoContent();
        }

        // DELETE: api/TaskItem/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTaskItem(long id)
        {
            var taskItem = await _taskItemRepository.GetById(id);

            if (taskItem == null)
            {
                return NotFound();
            }

            _taskItemRepository.Delete(taskItem);
            await _taskItemRepository.SaveAsync();

            return NoContent();
        }
    }
}
