﻿using TestToDoListWebApi.Models;
using TestToDoListWebApi.Repositories.Interfaces;

namespace TestToDoListWebApi.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ToDoContext context)
            : base(context)
        {
        }
    }
}
