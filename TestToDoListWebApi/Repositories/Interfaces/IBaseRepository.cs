﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestToDoListWebApi.Models;

namespace TestToDoListWebApi.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseModel
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        Task<TEntity> GetById(long id);
        Task SaveAsync();
    }
}
