﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestToDoListWebApi.Models;

namespace TestToDoListWebApi.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}
