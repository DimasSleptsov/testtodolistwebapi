﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestToDoListWebApi.Models;
using TestToDoListWebApi.Repositories.Interfaces;

namespace TestToDoListWebApi.Repositories
{
    public class TaskItemRepository : BaseRepository<TaskItem>, ITaskItemRepository
    {
        public TaskItemRepository(ToDoContext context) 
            : base(context) 
        {
        }
    }
}
